﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _07WindowsFormsApplicationRestaurant
{
    public partial class Form1 : Form
    {
        private Restaurante miRestaurante;
        
        public Form1()
        {
            InitializeComponent();
            miRestaurante = new Restaurante();
        }

        private void buttonMLibres_Click(object sender, EventArgs e)
        {
            char estado;
            int contador = 0;
            Mesa aux;
            for (int i = 0; i < 4; i++)
            {
                aux = miRestaurante.getMisMesas(0);
                estado = aux.getEstado();
                if (estado == 'L')
                    contador++;
            }
            miRestaurante.setNMesasL(contador);
            labelResultado.Text = "El Nº de mesas libres son: " + contador + "\n" + 
                                    "Numero de mesas " + miRestaurante.getNMesasL();
        }

        private void buttonOcupar_Click(object sender, EventArgs e)
        {
            int nMesa = Int32.Parse(textBoxNmesa.Text);
            int nOcupantesMax = Int32.Parse(textBoxOCmax.Text);
            int nOcupantesAct = Int32.Parse(textBoxOCActual.Text);
            string horaE = textBoxHora.Text;

            if (nMesa == 1)
                mesa1.ocupar(ref nOcupantesMax, ref nOcupantesAct, ref horaE);
            if (nMesa == 2)
                mesa2.ocupar(ref nOcupantesMax, ref nOcupantesAct, ref horaE);
            if (nMesa == 3)
                mesa3.ocupar(ref nOcupantesMax, ref nOcupantesAct, ref horaE);
            if (nMesa == 4)
                mesa4.ocupar(ref nOcupantesMax, ref nOcupantesAct, ref horaE);

            labelResultado.Text = "Puedes pasar " + "\n" + nOcupantesAct + "\n" + nOcupantesMax + "\n" + horaE;
        }

        private void buttonCobrar_Click(object sender, EventArgs e)
        {
            int nMesa = Int32.Parse(textBoxNmesa.Text);
            double total = 0;

            if (nMesa == 1)
                mesa1.caja(ref total);
            if (nMesa == 2)
                mesa1.caja(ref total);
            if (nMesa == 3)
                mesa1.caja(ref total);
            if (nMesa == 4)
                mesa1.caja(ref total);
            miRestaurante.setDinero(total);

            labelResultado.Text = "Dinero cobrado " + total + "\nDinero en caja " + miRestaurante.getDinero();


        }


    }
}
