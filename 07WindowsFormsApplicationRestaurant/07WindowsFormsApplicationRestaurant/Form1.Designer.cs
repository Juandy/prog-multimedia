﻿namespace _07WindowsFormsApplicationRestaurant
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonMLibres = new System.Windows.Forms.Button();
            this.labelResultado = new System.Windows.Forms.Label();
            this.buttonOcupar = new System.Windows.Forms.Button();
            this.textBoxOCmax = new System.Windows.Forms.TextBox();
            this.textBoxOCActual = new System.Windows.Forms.TextBox();
            this.textBoxHora = new System.Windows.Forms.TextBox();
            this.textBoxNmesa = new System.Windows.Forms.TextBox();
            this.buttonCobrar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonMLibres
            // 
            this.buttonMLibres.Location = new System.Drawing.Point(12, 29);
            this.buttonMLibres.Name = "buttonMLibres";
            this.buttonMLibres.Size = new System.Drawing.Size(130, 54);
            this.buttonMLibres.TabIndex = 0;
            this.buttonMLibres.Text = "Mesas Libres";
            this.buttonMLibres.UseVisualStyleBackColor = true;
            this.buttonMLibres.Click += new System.EventHandler(this.buttonMLibres_Click);
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(237, 50);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(35, 13);
            this.labelResultado.TabIndex = 1;
            this.labelResultado.Text = "label1";
            // 
            // buttonOcupar
            // 
            this.buttonOcupar.Location = new System.Drawing.Point(12, 89);
            this.buttonOcupar.Name = "buttonOcupar";
            this.buttonOcupar.Size = new System.Drawing.Size(130, 58);
            this.buttonOcupar.TabIndex = 2;
            this.buttonOcupar.Text = "Ocupar mesa";
            this.buttonOcupar.UseVisualStyleBackColor = true;
            this.buttonOcupar.Click += new System.EventHandler(this.buttonOcupar_Click);
            // 
            // textBoxOCmax
            // 
            this.textBoxOCmax.Location = new System.Drawing.Point(12, 179);
            this.textBoxOCmax.Name = "textBoxOCmax";
            this.textBoxOCmax.Size = new System.Drawing.Size(143, 20);
            this.textBoxOCmax.TabIndex = 3;
            // 
            // textBoxOCActual
            // 
            this.textBoxOCActual.Location = new System.Drawing.Point(12, 205);
            this.textBoxOCActual.Name = "textBoxOCActual";
            this.textBoxOCActual.Size = new System.Drawing.Size(143, 20);
            this.textBoxOCActual.TabIndex = 4;
            // 
            // textBoxHora
            // 
            this.textBoxHora.Location = new System.Drawing.Point(12, 231);
            this.textBoxHora.Name = "textBoxHora";
            this.textBoxHora.Size = new System.Drawing.Size(143, 20);
            this.textBoxHora.TabIndex = 5;
            // 
            // textBoxNmesa
            // 
            this.textBoxNmesa.Location = new System.Drawing.Point(12, 153);
            this.textBoxNmesa.Name = "textBoxNmesa";
            this.textBoxNmesa.Size = new System.Drawing.Size(143, 20);
            this.textBoxNmesa.TabIndex = 6;
            // 
            // buttonCobrar
            // 
            this.buttonCobrar.Location = new System.Drawing.Point(12, 257);
            this.buttonCobrar.Name = "buttonCobrar";
            this.buttonCobrar.Size = new System.Drawing.Size(130, 55);
            this.buttonCobrar.TabIndex = 7;
            this.buttonCobrar.Text = "Cobrar";
            this.buttonCobrar.UseVisualStyleBackColor = true;
            this.buttonCobrar.Click += new System.EventHandler(this.buttonCobrar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 447);
            this.Controls.Add(this.buttonCobrar);
            this.Controls.Add(this.textBoxNmesa);
            this.Controls.Add(this.textBoxHora);
            this.Controls.Add(this.textBoxOCActual);
            this.Controls.Add(this.textBoxOCmax);
            this.Controls.Add(this.buttonOcupar);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.buttonMLibres);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonMLibres;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button buttonOcupar;
        private System.Windows.Forms.TextBox textBoxOCmax;
        private System.Windows.Forms.TextBox textBoxOCActual;
        private System.Windows.Forms.TextBox textBoxHora;
        private System.Windows.Forms.TextBox textBoxNmesa;
        private System.Windows.Forms.Button buttonCobrar;
    }
}

