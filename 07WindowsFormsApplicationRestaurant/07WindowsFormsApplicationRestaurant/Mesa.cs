﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07WindowsFormsApplicationRestaurant
{
    class Mesa
    {
        private int ocMax;
        private int ocAct;
        private char Estado;//(L)libre, (R)reservado, (O)Ocupado
        private string horaOc;
        private char MPago;//(E)Efectivo, (T)tarjeta


        public Mesa()
        {
            ocMax = 0;
            ocAct = 0;
            Estado = 'L';
            horaOc = "0";
            MPago = ' ';
        }
        public Mesa(int max, int act, char e, string hora, char pago)
        {
            ocMax = max;
            ocAct = act;
            Estado = e;
            horaOc = hora;
            MPago = pago;
        }
        public void ocupar(ref int nOcM, ref int nOcA, ref string tiempo)
        {
            Estado = 'O';
            ocMax = nOcM;
            ocAct = nOcA;
            horaOc = tiempo;
        }
        public void caja(ref double total)
        {
            total = ocAct * 10;

            Estado = 'L';
            ocMax = 0;
            ocAct = 0;
            horaOc = "0";
        }

        public char getEstado()
        {          
            return Estado;
        }


    }
}
