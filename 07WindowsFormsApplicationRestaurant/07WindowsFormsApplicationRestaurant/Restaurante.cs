﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07WindowsFormsApplicationRestaurant
{
    class Restaurante
    {
        //private Mesa mesas;
        private int NMesasL;
        private double dinero;
        private Mesa[] misMesas = new Mesa[4];

        public Restaurante()
        {
            NMesasL = 0;
            dinero = 0;
            misMesas[0]= new Mesa();
            misMesas[1] = new Mesa();
            misMesas[2] = new Mesa();
            misMesas[3] = new Mesa();
        }


        public Mesa getMisMesas(int nMesas)
        {
            return misMesas[nMesas];
        }
        public int getNMesasL()
        {
            return NMesasL;
        }
        public void setNMesasL (int n)
        {
            NMesasL += n;
        }
        public double getDinero()
        {
            return dinero;
        }
        public void setDinero(double n)
        {
            dinero += n;
        }
    }  
}
