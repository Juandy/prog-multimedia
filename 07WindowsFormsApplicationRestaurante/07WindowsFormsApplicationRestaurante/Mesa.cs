﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07WindowsFormsApplicationRestaurante
{
    public enum EstadoMesa {Libre, Reservada, Ocupada};
    class Mesa
    {
        //Atributo
        private int nOcupantesMax, nOcupantesAct;
        private EstadoMesa estado;
        private string HoraOcupacion;

        //Constructores
        public Mesa()
        {
            nOcupantesMax = 4;
            nOcupantesAct = 0;
            estado = EstadoMesa.Libre;
            HoraOcupacion = " ";
        }

        public Mesa(int nOcMax, int nOcAct, EstadoMesa e, string horaO)
        {
            nOcupantesMax = nOcMax;
            nOcupantesAct = nOcAct;
            estado = e;
            HoraOcupacion = horaO;
        }
        //Ocupar, Cobrar

        public void Ocupar(int nOcupantes, string horaO)
        {
            if((nOcupantes <= nOcupantesMax) && (estado == EstadoMesa.Libre))
            {
                estado = EstadoMesa.Ocupada;
                nOcupantesAct = nOcupantes;
                HoraOcupacion = horaO;
            }
        }

        public void Cobrar()
        {
            if(estado == EstadoMesa.Ocupada)
            {
                estado = EstadoMesa.Libre;
                HoraOcupacion = "";
            }
        }
    }
}
