﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Los jugadores, tienen un nombre, una edad y una puntuación de media, 
//por problemas con los servidores, este ultimo dato se a perdido asique nos han informado que les demos un número aleatorio (entre 0 y 100).
namespace _010WindowsFormsApplicationFutbol
{
    class Jugadores
    {
        private string Nombre;
        private int Edad;
        private int Media;
        private bool Lesion;

        public Jugadores()
        {
            Nombre = " ";
            Edad = 0;
            Media = 0;
            Lesion = false;
        }

        public Jugadores(string nm, int e, int m, bool l)
        {
            Nombre = nm;
            Edad = e;
            Media = m;
            Lesion = l;
        }

        public int getMedia()
        {
            return Media;
        }
        public bool getLesion()
        {
            return Lesion;
        }
    }


}
