﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _010WindowsFormsApplicationFutbol
{
    public partial class Form1 : Form
    {
        private Equipos equi, equi2;
    
        public Form1()
        {
            InitializeComponent();

        }



        private void buttonMostrar_Click(object sender, EventArgs e)
        {
            Jugadores[] jugadores = new Jugadores[4];
            Jugadores[] jugadores2 = new Jugadores[4];

            int mediaTotal = 0, mediaFinal1=0;
            int mediaTotal2 = 0;
            int resta = 0;

            jugadores[0] = new Jugadores("paco", 35, 90, false);
            jugadores[1] = new Jugadores("pepe", 20, 89, false);
            jugadores[2] = new Jugadores("pedro", 18, 75, true);
            jugadores[3] = new Jugadores("popeye", 30, 50, false);

            jugadores2[0] = new Jugadores("manolo", 40, 100, true);
            jugadores2[1] = new Jugadores("memo", 22, 35, false);
            jugadores2[2] = new Jugadores("martin", 24, 80, false);
            jugadores2[3] = new Jugadores("marcos", 19, 50, true);

            equi = new Equipos("Barcelona", 10, jugadores);
            equi2 = new Equipos("Madrid", 5, jugadores2);

            for(int i = 0; i < jugadores.Length; i++)
            {
                for (int l = 0; l < jugadores.Length; l++)
                {
                    mediaTotal += jugadores[i].getMedia();

                    if ((jugadores[l].getLesion() == true) || (jugadores[l].getMedia() > 10))
                    {
                        mediaTotal += 10;
                    }
                }
                
            }
            for (int i = 0; i < jugadores2.Length; i++)
            {
                mediaTotal2 += jugadores2[i].getMedia();
            }

            labelResultado.Text = resta +  "Media Barcelona " + mediaFinal1 + "\n" +
                                   "Media Madrid " + mediaTotal2 ;        
        }
    }
}
