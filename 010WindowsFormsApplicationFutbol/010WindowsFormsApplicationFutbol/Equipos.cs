﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Los equipos tienen una ciudad y un numero de victorias conseguidos hasta el momento.
//Además contienen un listado de todos los jugadores (11).
namespace _010WindowsFormsApplicationFutbol
{
    class Equipos
    {
        private string ciudad;
        private int nVictorias;
        private Jugadores [] listadoJugadores = new Jugadores[4];
        public Equipos()
        {
            ciudad = " ";
            nVictorias = 0;
            listadoJugadores[0] = new Jugadores();
            listadoJugadores[1] = new Jugadores();
            listadoJugadores[2] = new Jugadores();
            listadoJugadores[3] = new Jugadores();
        }
        public Equipos(string cd, int victorias, Jugadores [] listaJ)
        {
            ciudad = cd;
            nVictorias = victorias;
            listadoJugadores[0] = listaJ[0];
            listadoJugadores[1] = listaJ[1];
            listadoJugadores[2] = listaJ[2];
            listadoJugadores[3] = listaJ[3];
        }
    }
}
