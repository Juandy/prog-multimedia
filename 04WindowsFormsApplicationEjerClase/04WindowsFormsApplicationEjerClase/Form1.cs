﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _04WindowsFormsApplicationEjerClase
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonInicializar_Click(object sender, EventArgs e)
        {
            textBoxCadena.Text = " "; 
        }

        private void buttonEscribir_Click(object sender, EventArgs e)
        {
            labelResultado.Text = "La cadena es: " + textBoxCadena.Text;
        }

        private void buttonInvertir_Click(object sender, EventArgs e)
        {
            string cadena = textBoxCadena.Text;                             // cadena sera igual que el campo resultado, si fuese el campo texto solo se podria invertir la frase una vez
            char[] letra = cadena.ToCharArray();                            // Aqui se transformara la cadena a caracteres
            Array.Reverse(letra);                                           // Aqui revierte el orden de la cadena
            labelResultado.Text = new string(letra);                        // Finalmente el texto saldra por pantalla

        }

        private void buttonTodoMay_Click(object sender, EventArgs e)
        {
           //textBoxCadena.CharacterCasing = CharacterCasing.Upper;          //  CharacterCasing.Upper es la funcion para hacer mayusculas las letras
           // Clipboard.SetText(textBoxCadena.Text);                          // Esto copiara el texto del campo texto (donde escribire)
           // labelResultado.Text = Clipboard.GetText();                      // Copiara el texto copiado en el campo resultado para mostrarlo por pantalla

           //V2
            string cadena = "";
            char[] tabla = textBoxCadena.Text.ToCharArray();
            for (int i = 0; i < tabla.Length; i++)
            {
                if ((tabla[i] >= 'a') && (tabla[i] <= 'z'))
                {
                    cadena += (char)(tabla[i] - 32);
                }
                else cadena += tabla[i];
            }
            labelResultado.Text = cadena;
        }

        private void buttonMini_Click(object sender, EventArgs e)
        {
           // textBoxCadena.CharacterCasing = CharacterCasing.Lower;          // CharacterCasing.Upper es la funcion para hacer mayusculas las letras
           // Clipboard.SetText(textBoxCadena.Text);                          // Esto copiara el texto del campo texto (donde escribire)
            //labelResultado.Text = Clipboard.GetText();                      // Copiara el texto copiado en el campo resultado para mostrarlo por pantalla
            
            //V2
            string cadena = "";
            char[] tabla = textBoxCadena.Text.ToCharArray();
            for (int i = 0; i < tabla.Length; i++)
            {
                if ((tabla[i] >= 'A') && (tabla[i] <= 'Z'))
                {
                    cadena += (char)(tabla[i] + 32);
                }
                else cadena += tabla[i];
            }
            labelResultado.Text = cadena;
        }

    

        private void buttonRD_Click(object sender, EventArgs e)
        {
            string cadena = labelResultado.Text;
            string temporal = cadena[0].ToString();                         // temporal cogera la primera palabra
            cadena = cadena.Remove(0, 1);                                   // Aqui la borramos
            cadena += temporal;                                             // y aqui "sumamos" la palabra recogida
            labelResultado.Text = cadena;
        }

        private void buttonRI_Click(object sender, EventArgs e)
        {
            string cadena = labelResultado.Text;
            labelResultado.Text = cadena.Substring(cadena.Length - 1) + 
                                  cadena.Remove(cadena.Length - 1);         // Quita la ultima palabra para ponerla en primera
        }
    }
}
