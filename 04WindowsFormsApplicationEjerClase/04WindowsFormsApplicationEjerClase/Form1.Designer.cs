﻿namespace _04WindowsFormsApplicationEjerClase
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxCadena = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.buttonInicializar = new System.Windows.Forms.Button();
            this.buttonEscribir = new System.Windows.Forms.Button();
            this.buttonInvertir = new System.Windows.Forms.Button();
            this.buttonTodoMay = new System.Windows.Forms.Button();
            this.buttonMini = new System.Windows.Forms.Button();
            this.buttonIZ = new System.Windows.Forms.Button();
            this.buttonDR = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxCadena
            // 
            this.textBoxCadena.Location = new System.Drawing.Point(12, 12);
            this.textBoxCadena.Name = "textBoxCadena";
            this.textBoxCadena.Size = new System.Drawing.Size(198, 22);
            this.textBoxCadena.TabIndex = 0;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(262, 18);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(45, 16);
            this.labelResultado.TabIndex = 1;
            this.labelResultado.Text = "label1";
            // 
            // buttonInicializar
            // 
            this.buttonInicializar.Location = new System.Drawing.Point(12, 40);
            this.buttonInicializar.Name = "buttonInicializar";
            this.buttonInicializar.Size = new System.Drawing.Size(101, 23);
            this.buttonInicializar.TabIndex = 2;
            this.buttonInicializar.Text = "Inicializar";
            this.buttonInicializar.UseVisualStyleBackColor = true;
            this.buttonInicializar.Click += new System.EventHandler(this.buttonInicializar_Click);
            // 
            // buttonEscribir
            // 
            this.buttonEscribir.Location = new System.Drawing.Point(12, 69);
            this.buttonEscribir.Name = "buttonEscribir";
            this.buttonEscribir.Size = new System.Drawing.Size(101, 23);
            this.buttonEscribir.TabIndex = 3;
            this.buttonEscribir.Text = "Escribir";
            this.buttonEscribir.UseVisualStyleBackColor = true;
            this.buttonEscribir.Click += new System.EventHandler(this.buttonEscribir_Click);
            // 
            // buttonInvertir
            // 
            this.buttonInvertir.Location = new System.Drawing.Point(12, 98);
            this.buttonInvertir.Name = "buttonInvertir";
            this.buttonInvertir.Size = new System.Drawing.Size(101, 23);
            this.buttonInvertir.TabIndex = 4;
            this.buttonInvertir.Text = "Invertir\r\n";
            this.buttonInvertir.UseVisualStyleBackColor = true;
            this.buttonInvertir.Click += new System.EventHandler(this.buttonInvertir_Click);
            // 
            // buttonTodoMay
            // 
            this.buttonTodoMay.Location = new System.Drawing.Point(12, 127);
            this.buttonTodoMay.Name = "buttonTodoMay";
            this.buttonTodoMay.Size = new System.Drawing.Size(101, 23);
            this.buttonTodoMay.TabIndex = 5;
            this.buttonTodoMay.Text = "Mayusculas";
            this.buttonTodoMay.UseVisualStyleBackColor = true;
            this.buttonTodoMay.Click += new System.EventHandler(this.buttonTodoMay_Click);
            // 
            // buttonMini
            // 
            this.buttonMini.Location = new System.Drawing.Point(12, 156);
            this.buttonMini.Name = "buttonMini";
            this.buttonMini.Size = new System.Drawing.Size(101, 23);
            this.buttonMini.TabIndex = 6;
            this.buttonMini.Text = "Minusculas";
            this.buttonMini.UseVisualStyleBackColor = true;
            this.buttonMini.Click += new System.EventHandler(this.buttonMini_Click);
            // 
            // buttonIZ
            // 
            this.buttonIZ.Location = new System.Drawing.Point(12, 185);
            this.buttonIZ.Name = "buttonIZ";
            this.buttonIZ.Size = new System.Drawing.Size(101, 23);
            this.buttonIZ.TabIndex = 7;
            this.buttonIZ.Text = "Rotar I";
            this.buttonIZ.UseVisualStyleBackColor = true;
            this.buttonIZ.Click += new System.EventHandler(this.buttonRD_Click);
            // 
            // buttonDR
            // 
            this.buttonDR.Location = new System.Drawing.Point(12, 214);
            this.buttonDR.Name = "buttonDR";
            this.buttonDR.Size = new System.Drawing.Size(101, 23);
            this.buttonDR.TabIndex = 8;
            this.buttonDR.Text = "Rotar D";
            this.buttonDR.UseVisualStyleBackColor = true;
            this.buttonDR.Click += new System.EventHandler(this.buttonRI_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(591, 442);
            this.Controls.Add(this.buttonDR);
            this.Controls.Add(this.buttonIZ);
            this.Controls.Add(this.buttonMini);
            this.Controls.Add(this.buttonTodoMay);
            this.Controls.Add(this.buttonInvertir);
            this.Controls.Add(this.buttonEscribir);
            this.Controls.Add(this.buttonInicializar);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.textBoxCadena);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            //this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxCadena;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button buttonInicializar;
        private System.Windows.Forms.Button buttonEscribir;
        private System.Windows.Forms.Button buttonInvertir;
        private System.Windows.Forms.Button buttonTodoMay;
        private System.Windows.Forms.Button buttonMini;
        private System.Windows.Forms.Button buttonIZ;
        private System.Windows.Forms.Button buttonDR;
    }
}

