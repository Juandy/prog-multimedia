﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ConsoleApplication1Tipos
{
    class Program
    {
        static void Main(string[] args)
        {
            Type tipo;
            double numero;
            char letra;

            tipo = typeof(string);

            Console.WriteLine("El nombre corto es " + tipo.Name);
            Console.WriteLine("El nombre largo es " + tipo.FullName);
            if ((3 + 4) is Int32)
                Console.WriteLine("Es entero");
            else
                Console.WriteLine("No es entero");

            numero = 32.5;
            Console.WriteLine("El numero es " + numero);
            Console.WriteLine("El numero convertido es " + (int)numero);

            letra = 'e';
            Console.ReadKey();
        }
    }
}
