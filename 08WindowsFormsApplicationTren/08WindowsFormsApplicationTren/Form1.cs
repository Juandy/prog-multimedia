﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _08WindowsFormsApplicationTren
{
    public partial class Form1 : Form
    {
        private Vagon v1, v2, v3,v4,v5;
        public Form1()
        {
            InitializeComponent();
            
        }

        private void buttonMostraVagon_Click(object sender, EventArgs e)
        {
            int n1, n2;
            int per = 100, pes = 100;

            n1 = Int32.Parse(textBoxNOcupantes.Text);
            n2 = Int32.Parse(textBoxPMax.Text);

            v1 = new Vagon(n1, n2, true,false);
            v2 = new Vagon(per, pes, true,false);
            v3 = new Vagon(per, pes, false,false);

            v4 = v1.Union(v2);
            v5 = v4.Union(v3);


        }
    }
}
