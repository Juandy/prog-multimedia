﻿namespace _08WindowsFormsApplicationTren
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonMostraVagon = new System.Windows.Forms.Button();
            this.textBoxNOcupantes = new System.Windows.Forms.TextBox();
            this.textBoxPMax = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonMostraVagon
            // 
            this.buttonMostraVagon.Location = new System.Drawing.Point(12, 12);
            this.buttonMostraVagon.Name = "buttonMostraVagon";
            this.buttonMostraVagon.Size = new System.Drawing.Size(110, 42);
            this.buttonMostraVagon.TabIndex = 0;
            this.buttonMostraVagon.Text = "Mostrar";
            this.buttonMostraVagon.UseVisualStyleBackColor = true;
            this.buttonMostraVagon.Click += new System.EventHandler(this.buttonMostraVagon_Click);
            // 
            // textBoxNOcupantes
            // 
            this.textBoxNOcupantes.Location = new System.Drawing.Point(12, 60);
            this.textBoxNOcupantes.Name = "textBoxNOcupantes";
            this.textBoxNOcupantes.Size = new System.Drawing.Size(110, 20);
            this.textBoxNOcupantes.TabIndex = 1;
            // 
            // textBoxPMax
            // 
            this.textBoxPMax.Location = new System.Drawing.Point(12, 86);
            this.textBoxPMax.Name = "textBoxPMax";
            this.textBoxPMax.Size = new System.Drawing.Size(110, 20);
            this.textBoxPMax.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 336);
            this.Controls.Add(this.textBoxPMax);
            this.Controls.Add(this.textBoxNOcupantes);
            this.Controls.Add(this.buttonMostraVagon);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonMostraVagon;
        private System.Windows.Forms.TextBox textBoxNOcupantes;
        private System.Windows.Forms.TextBox textBoxPMax;
    }
}

