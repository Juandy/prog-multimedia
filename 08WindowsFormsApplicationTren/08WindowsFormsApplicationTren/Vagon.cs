﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08WindowsFormsApplicationTren
{
    class Vagon
    {
        private int nOcupantes; //ocupantes del vagon
        private int pMax;       //peso maximo del vagon
        private bool enganche,union; //enganche- nnos dice si el vagon se puede enganchar o no
                                     // union - si esta unido dos vagones se podran dividir

        public Vagon()      //Constructor que inicia los atributos
        {
            nOcupantes = 0;
            pMax = 0;
            enganche = true;
            union = false;
        }

        public Vagon(int ocupantes, int peso, bool enganchar, bool unir) //Constructor pasandole los atributos
        {
            nOcupantes = ocupantes;
            pMax = peso;
            enganche = enganchar;
            union = unir;
        }

        public Vagon Union(Vagon v) //Creacion de un nuevo vagon con la suma de los ocupantes y el peso de los 2 primeros vagones creados
        {
            Vagon nuevoV;
            bool enganchable = true;
            bool union = true;
            int total = nOcupantes + v.nOcupantes;
            int total2 = pMax + v.pMax;
            //Variables nuevas para la creacion de un nuevo vagon, los datos nuevos son la union de los dos vagones anteriores
            nuevoV = new Vagon(total, total2, enganchable, union);  //Creas el objeto nuevoV con el constructor pasando los parametros anteriores
            return nuevoV;
        }

        public Vagon Dividir (Vagon v)
        {
            Vagon nuevoV;
            bool enganchable = true;
            int total = nOcupantes / 2;
            int total2 = pMax / 2;
            bool union = false;

            nuevoV = new Vagon(total, total2, enganchable, union);
            return nuevoV;
        }

        public bool getUnion()
        {
            return union;
        }
    }

}
