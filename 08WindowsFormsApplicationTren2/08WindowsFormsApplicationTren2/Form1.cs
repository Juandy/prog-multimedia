﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _08WindowsFormsApplicationTren2
{
    public partial class Form1 : Form
    {
        private Vagon vagon1, vagon2, vagon3, vagon4, vagon5, vagon6,vagon7, vagon8, vagonNuevo, vagonNuevo2;
        public Form1()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            int n1, n2;

            n1 = Int32.Parse(textBoxOcupantes.Text);
            n2 = Int32.Parse(textBoxPmax.Text);

            vagon1 = new Vagon(n1, n2, true, false);
            vagon2 = new Vagon(50, 50, true, false);
            vagon3 = new Vagon(50, 50, true, false);
            vagon4 = vagon1.Union(vagon2);

            if ((vagon3.getEnganche() == true) && (vagon4.getEnganche() == true))
            {
                vagon5 = vagon4.Union(vagon3);
                labelResultado.Text = vagon5.getnOcupantes() + " " + vagon5.getpMax();
            }
            else
                labelResultado.Text = "union no posible";
            //vagon8.Division(ref vagon6, ref vagon7);
            //labelResultado.Text = vagon6.getnOcupantes() +" "+ vagon6.getpMax() + "\n" + vagon7.getnOcupantes() + vagon7.getpMax();
        }
    }
}
