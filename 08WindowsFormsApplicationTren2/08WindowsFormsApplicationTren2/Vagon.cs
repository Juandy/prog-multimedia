﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08WindowsFormsApplicationTren2
{
    class Vagon
    {
        private int nOcupantes, pMax;
        private bool Enganche, Unido;

        public Vagon()
        {
            nOcupantes = 0;
            pMax = 0;
            Enganche = true;
            Unido = true;
        }

        public Vagon (int ocupantes,  int peso, bool enganchable, bool union)
        {
            nOcupantes = ocupantes;
            pMax = peso;
            Enganche = enganchable;
            Unido = union;    
        }
        public Vagon Union (Vagon datos)
        {
            int nOcu = nOcupantes + datos.nOcupantes;
            int pesoM = pMax + datos.pMax;
            bool enganchable = true;
            bool union = true;
            Vagon vagonNuevo = new Vagon(nOcu, pesoM, enganchable, union);
            return vagonNuevo;
        }

        public void Division(ref Vagon vagonNuevo, ref Vagon vagonNuevo2)
        {
            int nOcu = nOcupantes / 2;
            int pesoM = pMax / 2;
            bool enganchable = true;
            bool union = true;
            vagonNuevo = new Vagon(nOcu, pesoM, enganchable, union);
            vagonNuevo2 = new Vagon(nOcu, pesoM, enganchable, union);
        }

        public int getnOcupantes()
        {
            return nOcupantes;
        }
        public int getpMax()
        {
            return pMax;
        }
        public bool getEnganche()
        {
            return Enganche;
        }
        public bool getUnido()
        {
            return Unido;
        }
    }
}
