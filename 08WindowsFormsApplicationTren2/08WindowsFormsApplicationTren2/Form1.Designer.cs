﻿namespace _08WindowsFormsApplicationTren2
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxOcupantes = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.textBoxPmax = new System.Windows.Forms.TextBox();
            this.button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxOcupantes
            // 
            this.textBoxOcupantes.Location = new System.Drawing.Point(12, 72);
            this.textBoxOcupantes.Name = "textBoxOcupantes";
            this.textBoxOcupantes.Size = new System.Drawing.Size(100, 20);
            this.textBoxOcupantes.TabIndex = 1;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(283, 29);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(35, 13);
            this.labelResultado.TabIndex = 2;
            this.labelResultado.Text = "label1";
            // 
            // textBoxPmax
            // 
            this.textBoxPmax.Location = new System.Drawing.Point(12, 98);
            this.textBoxPmax.Name = "textBoxPmax";
            this.textBoxPmax.Size = new System.Drawing.Size(100, 20);
            this.textBoxPmax.TabIndex = 3;
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(12, 29);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(88, 37);
            this.button.TabIndex = 0;
            this.button.Text = "Vagon";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 405);
            this.Controls.Add(this.textBoxPmax);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.textBoxOcupantes);
            this.Controls.Add(this.button);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxOcupantes;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.TextBox textBoxPmax;
        private System.Windows.Forms.Button button;
    }
}

