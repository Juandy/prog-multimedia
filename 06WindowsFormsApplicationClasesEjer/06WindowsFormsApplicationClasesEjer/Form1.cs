﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _06WindowsFormsApplicationClasesEjer
{
    public partial class Form1 : Form
    {
        //Atributos del formulario
        private ListaNumeros lista1;

        public Form1()
        {
            InitializeComponent();
            lista1 = new ListaNumeros();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            labelResultado.Text = " ";
            Random Ejer1 = new Random();

            for (int i = 0; i < 10; i++)
            {
                int numero = Ejer1.Next(3, 50);
                lista1.InsertarValor(numero);
                labelResultado.Text += (numero + " ");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            labelResultado.Text = " ";
            Random Ejer1 = new Random();

            for (int i = 0; i < 15; i++)
            {
                int numero = Ejer1.Next(0, 100);
                lista1.InsertarValor(numero);
                labelResultado.Text += (numero + " ");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            lista1.Vaciar();
            labelResultado.Text = "Sin Numeros";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            lista1.Vaciar();
            labelResultado.Text = "sin Numeros";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            labelResultado.Text = " ";
            int resultado = 0;
            int inverso = lista1.NumeroElementos();
            
            for(int i = inverso; i > 0 ; i--)
            {
                resultado = lista1.Elemento(i);
                labelResultado.Text += (resultado + " ");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            labelResultado.Text = " ";

            int resultado = lista1.Sacar();

            for (int i = 0; i < 10; i++) 
            {
                labelResultado.Text = resultado + " ";
            }
        }
    }
}
