﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06WindowsFormsApplicationClasesEjer
{
    class ListaNumeros
    {
        int[] nElementos = new int [100];
        int contadorElementos = 0;

        public ListaNumeros()
        {
            for (int i = 0; i < nElementos.Length; i++)
                nElementos[i] = 0;
            contadorElementos = 0;
        }

        public ListaNumeros(int repeticiones, int valor)
        {
            for (int i = 0; i < repeticiones; i++)
            {
                nElementos[i] = valor;
            }
            contadorElementos = 0;
        }

        public int NumeroElementos()
        {          
            return contadorElementos;
        }

        public void InsertarValor(int valor)
        {
            nElementos[contadorElementos] = valor;
            contadorElementos++;
        }

        public int Primero ()
        {
            return nElementos[0];
        }
        public int Ultimo()
        {
           return nElementos[contadorElementos -1];
        }

        public int Sacar()
        {
            int valor = Primero();

            int[] aux = nElementos;
            int contador = contadorElementos;
            for(int i = 1 ; i <= contadorElementos; i++)
            {
                nElementos[contadorElementos - i] = aux[contador];
                contador--;
            }
            contadorElementos--;

            return valor;
        }

        public int Elemento(int posicion)
        {
            int devuelve = 0;
            int comprueba = posicion;

            if (contadorElementos < comprueba)
                return -10000;

            if (posicion >= 1)
                devuelve = nElementos[posicion - 1];

            return devuelve;
        }

        public int Posicion (int valor)
        {
            int comprueba = 0;
            for (int i = 0; i<=contadorElementos; i++)
            {
                comprueba = nElementos[0];
                if (valor == comprueba)
                {
                    return i;
                }
            }
            return 0;
        }

        public Boolean Vacio()
        {
            if (contadorElementos == 0)
                return true;
            else
                return false;
        }

        public void Vaciar()
        {
            for (int i = 0; i < nElementos.Length; i++)
                nElementos[i] = 0;
            contadorElementos = 0;
        }


    }
}
