﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06WindowsFormsApplicationListaNumeros
{
    class ListaNumeros
    {
        private int nElementos; //Constructores
        private int[] lista;

        public ListaNumeros()
        {
            nElementos = 0;
            lista = new int [100]; 
            //inicializamos la tabla elementos a 0 y el numero de elementos a 0
        }
        public ListaNumeros(int numero, int valor)
        {
            nElementos = 0;
            lista = new int[100];

            for(int i = 0; i < numero; i++)
                lista[i] = valor;
            nElementos = numero;
            //Dos parametros numero y valor
        }

        //NumeroElementos():Devuelve la cantidad actual de números que contiene la lista de números
        public int NumeroElemetos()
        {
            return nElementos;
        }
        //Insertar(int valor) Añade a la lista de números el valor indicado e incrementa el número de elementos de la lista
        public void InsertarValor(int valor)
        {
            if (nElementos < 100) //Con el if comprobamos que la lista no se pase de 100
            { 
            lista[nElementos] = valor;
            nElementos++;
            }
        }
        //Primero() devuelve el valor del primer elemento de la lista de numeros
        public int Primero()
        {
            return lista[0];
        }
        //Ultimo() devuelve el valor del ultimo elemento de la lista de numeros
        public int Ultimo()
        {
            return lista[nElementos -1];
        }
        //Sacar() Devuelve el primer elemento de la lista de numeros, lo elimina de la lista, compactando todos los elementos de la lista
        //Ej 12, 11, 10
        //Sacar() -> devolveria 11, 10;

        public int Sacar()
        {
            int valor;
            valor = Primero();
            for(int i = 0; i<nElementos - 1; i++)
            {
                lista[i] = lista[i + 1];
            }
            nElementos--;
            return valor;
        }

        //Elementos(int posicion) Devuelve el elemento que ocupa la posicion posicion dentro de la lista numero. 
        //Si la posicion no es correcta o el elemento no existe, devulve el valor -1000
        public int Elemento(int posicion)
        {
            if ((nElementos > 0) && (posicion >= 0) && (posicion < nElementos))
                return lista[posicion];
            else
                return -100000;
        }

        //Posicion(int valor) Devuelve la posucion en la que se encuentra el valor dentro de la lista nuemero
        //(0 para la primera posisicon, 1 para la segunda etc..) si el valor no esta en la lista devuelve un -1

        public int Posicion (int valor)
        {
            if (nElementos > 0) //comprobamos que la lista contenga elementos
            {
                bool encontrado = false;
                int posicion = -1;
                for (int i = 0; (i < nElementos) && (encontrado == false); i++)
                {
                    if (lista[i] == valor)
                    {
                        encontrado = true;
                        posicion = i;
                    }
                }
                return posicion;
            }
            else
                return -1;
        }

        //Vacia() Devuelve verdadero si el numero de elementos de la lista es 0. en caso contrario devuelve falso

        public bool Vacia()
        {
            if (nElementos == 0)
                return true;
            else
                return false;
        }

        //Vaciar() Elimina todos los elementos de la lista e numeros existentes e inicializa el numero elementos

        public void Vaciar()
        {
            nElementos = 0;
            lista = new int[100];
        }

    }
}
