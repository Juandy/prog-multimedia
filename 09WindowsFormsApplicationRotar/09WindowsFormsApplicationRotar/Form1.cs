﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _09WindowsFormsApplicationRotar
{
    public partial class Form1 : Form
    {
        private string[] cadena = { "Pepe ", "Juan ", "Manolo ", "Ana ", "Julian ", "Lucas " };

        public Form1()
        {
            InitializeComponent();
            /*cadena = new string[6];
            cadena[0] = "Pepe";
            cadena[1] = "Juan";
            cadena[2] = "Manolo";
            cadena[3] = "Ana";
            cadena[5] = "Julian";
            cadena[6] = "Lucas";
            Tambien podemos poner la cadena de esta manera*/
        }

        public void rotar()
        {
            labelResultado.Text = " ";
            string[] aux = new string [6];

            for (int i = 0; i < cadena.Length; i++)
                aux[i] = cadena[i];

            for (int i = 0; i < cadena.Length; i++)
            {
                if (i == 5)
                    cadena[0] = aux[5];
                else
                    cadena[i+1] = aux[i];
            }
            
            for(int i = 0; i < cadena.Length; i++)
            {
                labelResultado.Text += cadena[i];
            }
        }

        private void buttonRotar_Click(object sender, EventArgs e)
        {
            
            rotar();

        }
    }
}
