﻿namespace _03WindowsFormsApplicationMetodos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSumar1 = new System.Windows.Forms.Button();
            this.textBoxN1 = new System.Windows.Forms.TextBox();
            this.textBoxN2 = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.buttonSumar2 = new System.Windows.Forms.Button();
            this.buttonSumar3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonSumar1
            // 
            this.buttonSumar1.Location = new System.Drawing.Point(6, 6);
            this.buttonSumar1.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.buttonSumar1.Name = "buttonSumar1";
            this.buttonSumar1.Size = new System.Drawing.Size(66, 20);
            this.buttonSumar1.TabIndex = 0;
            this.buttonSumar1.Text = "Sumar";
            this.buttonSumar1.UseVisualStyleBackColor = true;
            this.buttonSumar1.Click += new System.EventHandler(this.buttonSumar1_Click);
            // 
            // textBoxN1
            // 
            this.textBoxN1.Location = new System.Drawing.Point(81, 7);
            this.textBoxN1.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.textBoxN1.Name = "textBoxN1";
            this.textBoxN1.Size = new System.Drawing.Size(80, 24);
            this.textBoxN1.TabIndex = 1;
            // 
            // textBoxN2
            // 
            this.textBoxN2.Location = new System.Drawing.Point(81, 30);
            this.textBoxN2.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.textBoxN2.Name = "textBoxN2";
            this.textBoxN2.Size = new System.Drawing.Size(80, 24);
            this.textBoxN2.TabIndex = 2;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.BackColor = System.Drawing.SystemColors.Menu;
            this.labelResultado.Location = new System.Drawing.Point(185, 8);
            this.labelResultado.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(101, 16);
            this.labelResultado.TabIndex = 3;
            this.labelResultado.Text = "El resultado es: ";
            // 
            // buttonSumar2
            // 
            this.buttonSumar2.Location = new System.Drawing.Point(6, 34);
            this.buttonSumar2.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.buttonSumar2.Name = "buttonSumar2";
            this.buttonSumar2.Size = new System.Drawing.Size(66, 20);
            this.buttonSumar2.TabIndex = 0;
            this.buttonSumar2.Text = "Sumar2";
            this.buttonSumar2.UseVisualStyleBackColor = true;
            this.buttonSumar2.Click += new System.EventHandler(this.buttonSumar2_Click);
            // 
            // buttonSumar3
            // 
            this.buttonSumar3.Location = new System.Drawing.Point(6, 66);
            this.buttonSumar3.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.buttonSumar3.Name = "buttonSumar3";
            this.buttonSumar3.Size = new System.Drawing.Size(66, 20);
            this.buttonSumar3.TabIndex = 4;
            this.buttonSumar3.Text = "Sumar3";
            this.buttonSumar3.UseVisualStyleBackColor = true;
            this.buttonSumar3.Click += new System.EventHandler(this.buttonSumar3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumVioletRed;
            this.ClientSize = new System.Drawing.Size(443, 326);
            this.Controls.Add(this.buttonSumar3);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.textBoxN2);
            this.Controls.Add(this.textBoxN1);
            this.Controls.Add(this.buttonSumar2);
            this.Controls.Add(this.buttonSumar1);
            this.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSumar1;
        private System.Windows.Forms.TextBox textBoxN1;
        private System.Windows.Forms.TextBox textBoxN2;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button buttonSumar2;
        private System.Windows.Forms.Button buttonSumar3;
    }
}

