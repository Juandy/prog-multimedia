﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _03WindowsFormsApplicationMetodos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void imprimirNumeros(int n1, int n2)
        {
            labelResultado.Text += "\n N1 = " + n1 + " N2 = " + n2 + "\n";
        }
        private int sumarV1 (int n1, int n2)
        {
            int res;
                res = n1 + n2;
            n1 = 0;
            n2 = 0;
            return res;
        }
        private int sumarV2(int n1,ref int n2)
        {
            int res;
            res = n1 + n2;
            n1 = 0;
            n2 = 0;
            return res;
        }
        private void sumarV3(int n1, int n2, out int res)
        {
            res = n1 + n2;
            n1 = 0;
            n2 = 0;
        }
        private void buttonSumar1_Click(object sender, EventArgs e)
        {
            int numero1, numero2, resultado;
            numero1 = Int32.Parse(textBoxN1.Text);
            numero2 = Int32.Parse(textBoxN2.Text);
            imprimirNumeros(numero1, numero2);
            resultado = sumarV1(numero1, numero2);
            imprimirNumeros(numero1, numero2);
            labelResultado.Text += "La suma es " + resultado ;
        }

        private void buttonSumar2_Click(object sender, EventArgs e)
        {
            int numero1, numero2, resultado;
            numero1 = Int32.Parse(textBoxN1.Text);
            numero2 = Int32.Parse(textBoxN2.Text);
            imprimirNumeros(numero1, numero2);
            resultado = sumarV2(numero1, ref numero2);// cuando se pasa por referencia ponemos un ref
            imprimirNumeros(numero1, numero2);
            labelResultado.Text += "La suma es " + resultado;
        }

        private void buttonSumar3_Click(object sender, EventArgs e)
        {
            int numero1, numero2, resultado;
            numero1 = Int32.Parse(textBoxN1.Text);
            numero2 = Int32.Parse(textBoxN2.Text);
            imprimirNumeros(numero1, numero2);
            sumarV3(numero1, numero2, out resultado);
            imprimirNumeros(numero1, numero2);
            labelResultado.Text += "La suma es " + resultado;
        }
    }
}
