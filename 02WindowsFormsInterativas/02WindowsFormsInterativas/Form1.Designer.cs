﻿namespace _02WindowsFormsInterativas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnImprimir = new System.Windows.Forms.Button();
            this.textBoxNumero = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.labelResultado = new System.Windows.Forms.Label();
            this.buttonImprimirTabla = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnImprimir
            // 
            this.btnImprimir.Location = new System.Drawing.Point(273, 48);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(143, 26);
            this.btnImprimir.TabIndex = 0;
            this.btnImprimir.Text = "Imprimir Numeros";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxNumero
            // 
            this.textBoxNumero.Location = new System.Drawing.Point(66, 51);
            this.textBoxNumero.Name = "textBoxNumero";
            this.textBoxNumero.Size = new System.Drawing.Size(153, 23);
            this.textBoxNumero.TabIndex = 1;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(63, 90);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(0, 16);
            this.label.TabIndex = 2;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(69, 90);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(0, 16);
            this.labelResultado.TabIndex = 3;
            // 
            // buttonImprimirTabla
            // 
            this.buttonImprimirTabla.Location = new System.Drawing.Point(273, 80);
            this.buttonImprimirTabla.Name = "buttonImprimirTabla";
            this.buttonImprimirTabla.Size = new System.Drawing.Size(143, 26);
            this.buttonImprimirTabla.TabIndex = 0;
            this.buttonImprimirTabla.Text = "ImprimirTabla";
            this.buttonImprimirTabla.UseVisualStyleBackColor = true;
            this.buttonImprimirTabla.Click += new System.EventHandler(this.buttonImprimirTabla_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(526, 457);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.label);
            this.Controls.Add(this.textBoxNumero);
            this.Controls.Add(this.buttonImprimirTabla);
            this.Controls.Add(this.btnImprimir);
            this.Font = new System.Drawing.Font("Arial", 10F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Aplicacion de sentencias iterativas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.TextBox textBoxNumero;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button buttonImprimirTabla;
    }
}

